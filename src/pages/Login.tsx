import React, { useRef, useState } from "react";
import {
  IonAvatar,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonIcon,
  IonInput,
  IonLabel,
  IonPage,
  IonRow,
} from "@ionic/react";
import AlEmadiLogo from "../images/AlEmadiLogo.jpg";
import { personCircleOutline, lockClosedOutline } from "ionicons/icons";
import "./Login.css";
import { useHistory } from "react-router";
import axios from "axios";
import config from "../constant.json";

const Login: React.FC = () => {
  const history = useHistory();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>(
    ""
  );
  const [iserror, setIserror] = useState<boolean>(false);

  const clearFields = () => {
    setUsername("");
    setPassword("");
  };

  const AuthenticateUser = async () => {
    const loginData = {
      username: username,
      password: password,
    };

    const api = axios.create({
      baseURL: config.baseUrl,
    });

    api
      .post(config.loginAPI, loginData)
      .then((res) => {
        if (res.data) {
          setIserror(false);
          history.push("/Reports");
          clearFields();
        } else {
          setIserror(true);
          clearFields();
        }
      })
      .catch((error) => {
        setIserror(true);
        clearFields();
      });
  };

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol>
              <IonCard>
                <IonCardHeader>
                  <h1>
                    <b>COVID Reporting Tool</b>
                  </h1>
                  <IonAvatar class="profileImage-center">
                    <img src={AlEmadiLogo} />
                  </IonAvatar>
                </IonCardHeader>
                <IonCardContent>
                  <IonGrid>
                    <IonRow>
                      <IonCol size="10" offset="1">
                        <IonInput
                          placeholder="User Name"
                          clear-input={true}
                          autofocus={true}
                          value={username}
                          onIonChange={(e) => setUsername(e.detail.value!)}
                        >
                          <IonIcon
                            slot="start"
                            icon={personCircleOutline}
                          ></IonIcon>
                        </IonInput>
                      </IonCol>
                      <IonCol size="10" offset="1">
                        <IonInput
                          placeholder="Password"
                          clear-on-edit={true}
                          type="password"
                          value={password}
                          onIonChange={(e) => setPassword(e.detail.value!)}
                        >
                          <IonIcon
                            slot="start"
                            icon={lockClosedOutline}
                          ></IonIcon>
                        </IonInput>
                      </IonCol>
                    </IonRow>
                    <br></br>
                    <IonRow>
                      <IonCol>
                        <IonButton
                          expand="block"
                          onClick={AuthenticateUser}
                          disabled={username.length < 1 || password.length < 1}
                        >
                          LOGIN
                        </IonButton>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {iserror && (
                          <IonLabel color="danger" className="ion-text-wrap">
                            Invalid username / password. Please try again.
                          </IonLabel>
                        )}
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Login;
