import React, { useRef, useState } from "react";
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonIcon,
  IonInput,
  IonLabel,
  IonLoading,
  IonPage,
  IonRow,
} from "@ionic/react";
import axios from "axios";
import config from "../constant.json";
import "./Reports.css";

import { powerOutline } from "ionicons/icons";
import { useHistory } from "react-router";

const Reports: React.FC = () => {
  const history = useHistory();
  const [showLoading, setShowLoading] = useState(false);

  const [input, setInput] = useState<string>("");
  const [iserror, setIserror] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>();
  const [message, setMessage] = useState<string>();
  const [isMessage, setIsMessage] = useState<boolean>();

  const GenerateReport = () => {
    setShowLoading(true);
    const api = axios.create({
      baseURL: config.baseUrl,
    });

    api
      .get(config.DownLoadReport + input)
      .then((res) => {
        if (res.data) {

          window.open(res.data.url);

          setIsMessage(true);
          setIserror(false);
          setMessage("Report is generated on a new tab");
          setShowLoading(false);
        }
      })
      .catch((error) => {
        console.log(error.response.status);
        if(error.response.status === 401){
          history.push("/Login");
        }
        setIserror(true);
        setIsMessage(false);
        setMessage("Error occured during downloading report");
        setShowLoading(false);
      });
  };

  const SendSMS = () => {
    setShowLoading(true);

    const params = {
      params: {
        id: input,
        url: "",
      },
    };
    const api = axios.create({
      baseURL: config.baseUrl,
    });

    api
      .post(config.SendSMS, params.params)
      .then((res) => {
        setIsMessage(true);
        setIserror(false);
        setMessage("SMS Sent Successfully");
        setShowLoading(false);
      })
      .catch((error) => {
        console.log(error.response.status);
        if(error.response.status === 401){
          history.push("/Login");
        }
        setIsMessage(false);
        setIserror(true);
        setErrorMessage("Error occured during sending SMS");
        setShowLoading(false);
      });
  };

  const LogOut = () => {
    const api = axios.create({
      baseURL: config.baseUrl,
    });

    api
      .get(config.Logout)
      .then((res) => {
        history.push("/Login");
      })
      .catch((error) => {
        history.push("/Login");
      });
  };

  return (
    <IonPage>
      <IonContent class="ionContent">
        <IonGrid class="maingrid">
          <IonRow>
            <IonCol>
              <IonCard>
                <IonCardHeader class="cardHeader">
                  <IonGrid>
                    <IonRow>
                      <IonCol push="11">
                        <IonButton
                          fill="clear"
                          class="logoutButton"
                          shape="round"
                          onClick={LogOut}
                        >
                          <IonIcon slot="icon-only" icon={powerOutline} />
                        </IonButton>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {" "}
                        <h1>
                          <b>COVID Reporting Tool</b>
                        </h1>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </IonCardHeader>
                <IonCardContent>
                  <IonGrid>
                    <IonRow>
                      <IonCol size="10" offset="1">
                        <IonInput
                          placeholder="Enter User Name or ID"
                          clear-input={true}
                          autofocus={true}
                          value={input}
                          onIonChange={(e) => setInput(e.detail.value!)}
                        ></IonInput>
                      </IonCol>
                    </IonRow>
                    <br></br>
                    <IonRow>
                      <IonCol>
                        <IonButton
                          expand="block"
                          onClick={GenerateReport}
                          disabled={input.length < 1}
                        >
                          Download Covid Report
                        </IonButton>
                      </IonCol>
                      <IonCol>
                        <IonButton
                          expand="block"
                          onClick={SendSMS}
                          disabled={input.length < 1}
                        >
                          SMS Covid Report
                        </IonButton>
                      </IonCol>
                    </IonRow>
                    {iserror && (
                      <IonRow>
                        <IonCol>
                          <IonLabel color="danger" className="ion-text-wrap">
                            {errorMessage}
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                    )}
                    {isMessage && (
                      <IonRow>
                        <IonCol>
                          <IonLabel color="primary" className="ion-text-wrap">
                            {message}
                          </IonLabel>
                        </IonCol>
                      </IonRow>
                    )}
                  </IonGrid>
                  <IonLoading
                    cssClass="my-custom-class"
                    isOpen={showLoading}
                    onDidDismiss={() => setShowLoading(false)}
                    message={"Please wait..."}
                    duration={5000}
                  />
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Reports;
